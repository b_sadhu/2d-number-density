# 2D Number density 

from gromacs densmap xpm output.
use this command to generate xpm file
gmx_mpi_d densmap -f nvt-nopbc-100ns.xtc -s nvt.tpr -n rdf.ndx -o c5tbp-2dnumdens-10ns-binp02-dmax15.xpm -b 90000 -e 100000 -dmax (value) -bin 0.02
Then, use this notebook. 

